from abc import abstractmethod

from utils.etcd_client.etcd_client import EtcdClient
from utils.process_package.base_package import BasePackage


class BaseProcess(BasePackage):
    def __init__(self, package_id):
        self.package_id = package_id
        super().__init__(self.get_root_path())
        self.etcd_client = EtcdClient()

    @abstractmethod
    def get_root_path(self):
        pass

    @abstractmethod
    def process_template(self, **kwargs):
        pass

    @abstractmethod
    def process_instance(self, **kwargs):
        pass
