from utils.tosca_paser.base_template import BaseTemplate
from utils.tosca_paser.vnffg_template import VNFFGTemplate


class GroupTemplate(BaseTemplate):
    def __init__(self, template):
        self.vnffg = list()
        super().__init__(template)

    def _assign_template(self, template, name):
        if self.TOSCA_VNFFG == template.get(self.TYPE):
            self.vnffg.append(VNFFGTemplate(template, name))
