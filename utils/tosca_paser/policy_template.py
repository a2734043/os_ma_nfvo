from utils.tosca_paser.base_template import BaseTemplate
from utils.tosca_paser.vdu_scaling_template import VduScalingTemplate


class PolicyTemplate(BaseTemplate):
    def __init__(self, template):
        self.vdu_scaling = list()
        super().__init__(template)

    def _assign_template(self, template, name):
        if self.TOSCA_SCALING == template.get(self.TYPE):
            self.vdu_scaling.append(VduScalingTemplate(template, name))
