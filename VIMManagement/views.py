from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from VIMManagement.utils.base_kubernetes import ResourceResult
from VIMManagement.utils.compute_resources import ComputeResource

ComputeResource()
@csrf_exempt
@api_view(['GET'])
def kubernetes_resource(request):
    return Response(status=status.HTTP_200_OK, data=ResourceResult())
