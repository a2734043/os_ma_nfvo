from rest_framework import serializers
from utils.format_tools import transform_representation
from .models import *


class ResourceHandleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResourceHandle
        fields = ('vimId', 'resourceProviderId', 'resourceId', 'vimLevelResourceType')
        ref_name = "NsLcmOpOccSerializer_ResourceHandleSerializer"


class ExtLinkPortInfoSerializer(serializers.ModelSerializer):
    resourceHandle = ResourceHandleSerializer(source='extLinkPortInfo_resourceHandle')

    class Meta:
        model = ExtLinkPortInfo
        fields = ('id', 'resourceHandle', 'cpInstanceId')
        ref_name = 'NsLcmOpOccSerializer_ExtLinkPortInfoSerializer'


class ExtVirtualLinkInfoSerializer(serializers.ModelSerializer):
    resourceHandle = ResourceHandleSerializer()
    extLinkPorts = ExtLinkPortInfoSerializer(required=False, many=True)

    class Meta:
        model = ExtVirtualLinkInfo
        fields = ('id', 'resourceHandle', 'extLinkPorts')
        ref_name = 'NsLcmOpOccSerializer_ExtVirtualLinkInfoSerializer'


class ModifyVnfInfoDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModifyVnfInfoData
        fields = ('vnfInstanceId', 'vnfInstanceName', 'vnfInstanceDescription', 'vnfdId',
                  'vnfConfigurableProperties', 'metadata', 'extensions')


class ChangedInfoSerializer(serializers.ModelSerializer):
    changedVnfInfo = ModifyVnfInfoDataSerializer(required=False)
    changedExtConnectivity = ExtVirtualLinkInfoSerializer(required=False, many=True)

    class Meta:
        model = ChangedInfo
        fields = ('changedVnfInfo', 'changedExtConnectivity')


class AffectedSapSerializer(serializers.ModelSerializer):
    class Meta:
        model = AffectedSap
        fields = ('sapInstanceId', 'sapdId', 'sapName', 'changeType', 'changeResult')


class AffectedNsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AffectedNs
        fields = ('nsInstanceId', 'nsdId', 'changeType', 'changeResult')


class AffectedVnffgSerializer(serializers.ModelSerializer):
    class Meta:
        model = AffectedVnffg
        fields = ('vnffgInstanceId', 'vnffgdId', 'changeType', 'changeResult')


class AffectedVirtualLinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = AffectedVirtualLink
        fields = ('nsVirtualLinkInstanceId', 'nsVirtualLinkDescId', 'vlProfileId', 'changeType', 'changeResult')


class AffectedVnfSerializer(serializers.ModelSerializer):
    changedInfo = ChangedInfoSerializer(required=False)

    class Meta:
        model = AffectedVnf
        fields = ('vnfInstanceId', 'vnfdId', 'vnfProfileId', 'vnfName', 'changeType', 'changeResult', 'changedInfo')


class LinkSerializer(serializers.ModelSerializer):
    self = serializers.CharField(source='link_self')

    class Meta:
        model = Links
        fields = ('self', 'nsInstance', 'cancel', 'retry', 'rollback', '_continue', 'fail')

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['continue'] = ret['_continue']
        ret.pop('_continue')
        return ret


class ResourceChangesSerializer(serializers.ModelSerializer):
    affectedVnfs = AffectedVnfSerializer(required=False, many=True)
    affectedVls = AffectedVirtualLinkSerializer(required=False, many=True)
    affectedVnffgs = AffectedVnffgSerializer(required=False, many=True)
    affectedNss = AffectedNsSerializer(required=False, many=True)
    affectedSaps = AffectedSapSerializer(required=False, many=True)

    class Meta:
        model = ResourceChanges
        fields = ('affectedVnfs', 'affectedVls', 'affectedVnffgs', 'affectedNss', 'affectedSaps')


class NsLcmOpOccSerializer(serializers.ModelSerializer):
    resourceChanges = ResourceChangesSerializer(required=False)
    _links = LinkSerializer(required=False)

    def to_representation(self, instance):
        return transform_representation(super().to_representation(instance))

    class Meta:
        model = NsLcmOpOcc
        fields = '__all__'
