from django.urls import path, include
from rest_framework.routers import DefaultRouter

from NSFaultSubscription.views import NSFaultSubscriptionViewSet

router = DefaultRouter()
router.register(r'subscriptions', NSFaultSubscriptionViewSet)

urlpatterns = [
    path('nsfm/v1/', include(router.urls)),
]
