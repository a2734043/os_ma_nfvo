from django.urls import path, include
from rest_framework.routers import DefaultRouter
from VnfPackageManagement import views

router = DefaultRouter()
router.register(r'vnf_packages', views.VNFPackagesViewSet)

urlpatterns = [
    path('vnfpkgm/v1/', include(router.urls)),
]
