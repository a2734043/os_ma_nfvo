from django.urls import path, include
from rest_framework.routers import DefaultRouter
from NSDManagement import views

router = DefaultRouter()
router.register(r'ns_descriptors', views.NSDescriptorsViewSet)

urlpatterns = [
    path('nsd/v1/', include(router.urls)),
]
