from django.db import models
import uuid


class LccnSubscription(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    callbackUri = models.TextField()


class LccnSubscriptionLink(models.Model):
    _links = models.OneToOneField(LccnSubscription,
                                  on_delete=models.CASCADE,
                                  primary_key=True,
                                  related_name='_links')

    link_self = models.URLField()


class LifecycleChangeNotificationsFilter(models.Model):
    filter = models.OneToOneField(LccnSubscription,
                                  on_delete=models.CASCADE,
                                  primary_key=True,
                                  related_name='filter')
    notificationTypes = models.TextField(null=True, blank=True)
    operationTypes = models.TextField(null=True, blank=True)
    operationStates = models.TextField(null=True, blank=True)
    nsComponentTypes = models.TextField(null=True, blank=True)
    lcmOpNameImpactingNsComponent = models.TextField(null=True, blank=True)
    lcmOpOccStatusImpactingNsComponent = models.TextField(null=True, blank=True)


class NsInstanceSubscriptionFilter(models.Model):
    nsInstanceSubscriptionFilter = models.OneToOneField(LifecycleChangeNotificationsFilter,
                                                        on_delete=models.CASCADE,
                                                        primary_key=True,
                                                        related_name='nsInstanceSubscriptionFilter')
    nsdIds = models.TextField(null=True, blank=True)
    vnfdIds = models.TextField(null=True, blank=True)
    pnfdIds = models.TextField(null=True, blank=True)
    nsInstanceIds = models.TextField(null=True, blank=True)
    nsInstanceNames = models.TextField(null=True, blank=True)
