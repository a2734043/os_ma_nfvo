from rest_framework import serializers

from utils.format_tools import transform_representation
from .models import *


class NsInstanceSubscriptionFilterSerializer(serializers.ModelSerializer):
    class Meta:
        model = NsInstanceSubscriptionFilter
        fields = ('nsdIds', 'vnfdIds', 'pnfdIds', 'nsInstanceIds', 'nsInstanceNames')
        ref_name = 'LccnSubscriptionSerializer_NsInstanceSubscriptionFilterSerializer'

    def to_representation(self, instance):
        return transform_representation(super().to_representation(instance))


class LifecycleChangeNotificationsFilterSerializer(serializers.ModelSerializer):
    nsInstanceSubscriptionFilter = NsInstanceSubscriptionFilterSerializer(required=False)

    class Meta:
        model = LifecycleChangeNotificationsFilter
        fields = ('nsInstanceSubscriptionFilter', 'notificationTypes', 'operationTypes', 'operationStates',
                  'nsComponentTypes', 'lcmOpNameImpactingNsComponent', 'lcmOpOccStatusImpactingNsComponent')

    def to_representation(self, instance):
        return transform_representation(super().to_representation(instance))


class LccnSubscriptionLinkSerializer(serializers.ModelSerializer):
    self = serializers.CharField(source='link_self')

    class Meta:
        model = LccnSubscriptionLink
        fields = ('self',)


class LccnSubscriptionSerializer(serializers.ModelSerializer):
    _links = LccnSubscriptionLinkSerializer(required=False)
    filter = LifecycleChangeNotificationsFilterSerializer(required=False)

    class Meta:
        model = LccnSubscription
        fields = '__all__'

    def create(self, validated_data):
        filter_value = validated_data.pop('filter')
        link_value = validated_data.pop('_links')
        nsdm = LccnSubscription.objects.create(**validated_data)
        LccnSubscriptionLink.objects.create(_links=nsdm,
                                            **{'link_self': link_value['link_self'] + str(nsdm.id)})
        ns_instance_subscription_value = filter_value.pop('nsInstanceSubscriptionFilter')
        lifecycle_change_notifications_filter = LifecycleChangeNotificationsFilter.objects.create(
            filter=nsdm,
            **filter_value)
        NsInstanceSubscriptionFilter.objects.create(
            nsInstanceSubscriptionFilter=lifecycle_change_notifications_filter,
            **ns_instance_subscription_value)
        return nsdm
